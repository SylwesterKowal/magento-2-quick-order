<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\QuickOrder\Model\Config\Source;

class Source implements \Magento\Framework\Option\ArrayInterface
{


    public function __construct(
        \Magento\Inventory\Model\ResourceModel\Source\Collection $sourceCollection
    )
    {
        $this->sourceCollection = $sourceCollection;
    }

    public function toOptionArray()
    {
        $sources = [];
        $sourceListArr = $this->sourceCollection->load();
        foreach ($sourceListArr as $sourceItemName) {
            $sources[] = ['value' => $sourceItemName->getSourceCode(), 'label' => $sourceItemName->getName()];
        }

        return $sources; //[['value' => 'default', 'label' => __('default')],['value' => 'bl', 'label' => __('bl')]];
    }

    public function toArray()
    {
        $assoc = [];
        $sources = $this->toOptionArray();
        foreach ($sources as $source) {
            $assoc[$source['value']] = $source['label'];
        }
        return $assoc; // ['default' => __('default'), 'bl' => __('bl')];
    }
}