<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\QuickOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Config extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context              $context,
        \Magento\Framework\App\RequestInterface            $request,
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return $this->getConfigValue('quickorder/settings/enable', $storeId);
    }

    public function loggedInOnlly($storeId = null)
    {
        return $this->getConfigValue('quickorder/settings/loggedin', $storeId);
    }

    public function sourceCode($storeId = null)
    {
        return $this->getConfigValue('quickorder/settings/source', $storeId);
    }

    public function getCurrentStoreId(){
        return $this->storeManager->getStore()->getId();
    }

    public function getWebsiteCode(): ?string
    {
        try {
            $websiteCode = $this->storeManager->getWebsite()->getCode();
        } catch (LocalizedException $localizedException) {
            $websiteCode = null;
        }
        return $websiteCode;
    }

    public function getWebsiteId(): ?string
    {
        try {
            $websiteid = $this->storeManager->getWebsite()->getId();
        } catch (LocalizedException $localizedException) {
            $websiteid = null;
        }
        return $websiteid;
    }

    public function getConfigValue($field, $storeId = null)
    {
        if ($storeIdRequest = (int)$this->request->getParam('store', 0)) {
            $storeId = $storeIdRequest;
        } else if (is_null($storeId)) {
            $storeId = $this->storeManager->getStore()->getId();
        }

        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}