<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\QuickOrder\Controller\Index;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Save implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     */
    public function __construct(
        PageFactory                                                     $resultPageFactory,
        Json                                                            $json,
        LoggerInterface                                                 $logger,
        Http                                                            $http,
        \Magento\Customer\Model\Session                                 $customerSession,
        \Magento\Catalog\Api\ProductRepositoryInterface                 $productRepository,
        \Magento\Checkout\Model\Cart                                    $cart,
        \Magento\Framework\App\RequestInterface                         $request,
        \Magento\Framework\Data\Form\FormKey                            $formKey,
        \Magento\Inventory\Model\SourceItem\Command\GetSourceItemsBySku $getSourceItemsBySku,
        \Kowal\QuickOrder\Helper\Query                                  $query,
        \Magento\Framework\Session\SessionManagerInterface              $sessionManager,
        \Kowal\QuickOrder\Helper\Config                                 $config
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;

        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->cart = $cart;
        $this->request = $request;
        $this->formKey = $formKey;
        $this->getSourceItemsBySku = $getSourceItemsBySku;
        $this->query = $query;
        $this->sessionManager = $sessionManager;
        $this->config = $config;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            if (!$this->config->isEnabled()) {
                return $this->jsonResponse([
                    'success' => false,
                    'message' => __('Extension is disabled')
                ]);
            }

            if (!$this->customerSession->isLoggedIn() && $this->config->loggedInOnlly()) {
                return $this->jsonResponse([
                    'success' => false,
                    'message' => __('Customer is not logged in.')
                ]);
            }


            $products = $this->getArrayForCsv();
            $skiped = [];
            $souurce_code = $this->config->sourceCode();
            $souurce_codes = explode(',', $souurce_code);



            foreach ($products as $product_) {

                if ($this->query->_checkIfSkuExists($product_['sku'])) {
                    $product = $this->productRepository->get($product_['sku']);
                    $websiteIds = $product->getWebsiteIds();
                    if (in_array($this->config->getWebsiteId(), $websiteIds)) {
                        if ($product->getStatus()) {
                            $finalPrice = $product->getPriceInfo()->getPrice('final_price')->getValue();
                            $total_stock = 0;
                            $stocks = $this->getProductStockBySource($product->getSku());
                            foreach ($souurce_codes as $code) {
                                if (isset($stocks[$code])) {
                                    $total_stock += $stocks[$code];
                                }
                            }

                            $items = $this->cart->getQuote()->getAllItems();
                            $productInCartquantity = 0;
                            $isInCart = false;

                            foreach ($items as $item) {
                                if ($item->getProductId() == $product->getId()) {
                                    $productInCartquantity = $item->getQty();
                                    $isInCart = true;
                                    break;
                                }
                            }

                            if($isInCart){
                                $total_stock = $total_stock - $productInCartquantity;
                            }

                            if ($total_stock > 0) {
                                if ((int)$product_['qty'] > $total_stock) {
                                    $qty = $total_stock;
                                    $skiped[] = [
                                        'sku' => $product_['sku'],
                                        'name' => $product->getName(),
                                        'price' => $finalPrice,
                                        'qty' => $product_['qty'] - $total_stock,
                                        'info' => __('Ordered %1, available only %2', $product_['qty'], $total_stock)
                                    ];
                                } else {
                                    $qty = (int)$product_['qty'];
                                }

                                if ($product->isSalable()) {
                                    $params = [
                                        'form_key' => $this->formKey->getFormKey(),
                                        'product' => $product->getId(),
                                        'qty' => $qty,
                                        'price' => $finalPrice
                                    ];
                                    $this->cart->addProduct($product, $params);
                                    $this->cart->save();
                                }else{
                                    $skiped[] = ['sku' => $product_['sku'], 'name' => $product->getName(), 'price' => $finalPrice, 'qty' => $product_['qty'], 'info' => __('Product temporarily unavailable')];
                                }
                            }else{
                                $skiped[] = ['sku' => $product_['sku'], 'name' => $product->getName(), 'price' => $finalPrice, 'qty' => $product_['qty'], 'info' => __('Product temporarily unavailable')];
                            }
                        } else {
                            $skiped[] = ['sku' => $product_['sku'], 'name' => $product->getName(), 'price' => $finalPrice, 'qty' => $product_['qty'], 'info' => __('Product temporarily unavailable')];
                        }
                    } else {
                        $skiped[] = ['sku' => $product_['sku'], 'name' => '', 'price' => '', 'qty' => $product_['qty'], 'info' => __('The product is not assigned to a website')];
                    }
                } else {
                    $skiped[] = ['sku' => $product_['sku'], 'name' => '', 'price' => '', 'qty' => $product_['qty'], 'info' => __('Product does not exist')];
                }
            }

            $this->sessionManager->start();
            $this->sessionManager->setSkippedProductAtCart($skiped);
            return $this->jsonResponse([
                'success' => true,
                'message' => __('Done. Go to the shopping cart.') . " - " . $product->getStoreId()
            ]);
        } catch (LocalizedException $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        $this->http->setHeader('Cache-Control', '');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }

    private function getArrayForCsv()
    {
        $data = $this->request->getParam('products', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['sku'] = $row[0] ?? '';
                    unset($row[0]);

                    $row['qty'] = $row[1] ?? '';
                    unset($row[1]);

                    if (isset($row[2])) {
                        $row['price'] = $row[2] ?? '';
                        unset($row[2]);
                    }

                    return $row;
                },
                $data
            );
        }

        return $data;
    }

    public function getProductStockBySource($productSku)
    {
        $sourceItems = $this->getSourceItemsBySku->execute((string)$productSku);
        $stocks = [];
        foreach ($sourceItems as $item) {
            $stocks[$item->getSourceCode()] = (int)$item['quantity'];
        }
        return $stocks;
    }
}

