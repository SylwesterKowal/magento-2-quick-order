define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data'
], function ($, getTotalsAction, customerData) {
    'use strict';

    return function (config) {
        $(document).on('change', 'input[name$="[qty]"]', function () {
            var form = $('form#form-validate');
            var formData = form.serialize();

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formData,
                showLoader: true,
                success: function (res) {

                    var parsedResponse = $.parseHTML(res);
                    var result = $(parsedResponse).find("#form-validate");
                    $("#form-validate").replaceWith(result);

                    // The totals summary block reloading
                    var deferred = $.Deferred();
                    getTotalsAction([], deferred);

                    // The mini cart reloading
                    var sections = ['cart'];
                    customerData.invalidate(sections);
                    customerData.reload(sections, true);
                },
                error: function (xhr, status, error) {
                    console.log('Error occurred updating cart: ' + error);
                }
            });
        });
    };
});