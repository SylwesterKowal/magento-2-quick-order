<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\QuickOrder\Block;

class Skipped extends \Magento\Framework\View\Element\Template
{

    public $formKey;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context   $context,
        \Magento\Framework\Data\Form\FormKey               $formKey,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        array                                              $data = []
    )
    {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->sessionManager = $sessionManager;
    }


    public function getSkippedProducts()
    {
        $this->sessionManager->start();
        if($prodicts = $this->sessionManager->getSkippedProductAtCart()) {
            $this->sessionManager->unsSkippedProductAtCart();
            return $prodicts;
        }else{
            return false;
        }
    }
}

